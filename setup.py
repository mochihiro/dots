#!/usr/bin/env python
#
#
# +----------------------------+
# | $> python3 -m venv pyenv   |
# | $> pip install requests    |
# +----------------------------+

# TODO:
# * Check if program the executable exists before attemptig to configure, ie which fish, which tmux

import os
import sys
import requests
import shutil
import difflib

home                = os.environ['HOME']+'/'
fish_config         = 'https://raw.githubusercontent.com/tchnmf/config/master/fish/config.fish'
fish_vars           = 'https://raw.githubusercontent.com/tchnmf/config/master/fish/fish_variables'
fish_functions      = 'https://raw.githubusercontent.com/tchnmf/config/master/fish/functions/'
tmux_config         = 'https://raw.githubusercontent.com/tchnmf/config/master/tmux/tmux.conf'
vimrc               = 'https://raw.githubusercontent.com/tchnmf/config/master/vim/vimrc'
rc_conf             = 'https://raw.githubusercontent.com/tchnmf/config/master/ranger/rc.conf'


# def usage():
#     print()


def url_to_file(source_url,destination_file ):
    response = requests.get(source_url)
    file = open(destination_file, "w+")
    file.write(response.text)
    file.close() 

def which(command):
    print(shutil.which(command))

def setup_git():
    user_name = input('User name: ')
    user_email = input('User email: ')
    os.system("git config --global user.name  '%s' " % user_name)
    os.system("git config --global user.email  '%s' " % user_email)

def setup_fish():
    print('Configuring fish...')
    fish_config_path = home+'/.config/fish/config.fish'
    fish_vars_path = home+'/.config/fish/fish_variables'
    fish_functions_path = home+'/.config/fish/functions/'
    fish_functions_list = [
        'fish_prompt.fish',
    ]
    # randomize color if fish prompt ?

    url_to_file(fish_vars, fish_vars_path)
    url_to_file(fish_config, fish_config_path)

    for function in fish_functions_list:
        function_url = fish_functions + function
        function_name = function
        function_path = fish_functions_path + function_name
        # print(function_path, function_url)
        url_to_file(function_url, function_path)
    print('Done.')

def setup_tmux():
    print('Configuring tmux...')
    tmux_config_path = home+'/.tmux.conf'
    url_to_file(tmux_config, tmux_config_path)
    print('Done.')

def setup_vim():
    print('Configuring vim...')
    vimrc_path = home+'/.vimrc'
    url_to_file(vimrc, vimrc_path)
    print('Done.')

def setup_ranger():
    print('Configuring ranger...')
    rc_path = home+'/.config/ranger/rc.conf'
    url_to_file(rc_conf, rc_path)
    print('Done.')


def select():
    selected = input('select: (git|fish|tmux|vim|ranger):')

    match selected:
        case "git":
            setup_git()
        case "fish":
            setup_fish()
        case "tmux":
            setup_tmux()
        case "vim":
            which("vim")
            setup_vim()
        case "ranger":
            which("ranger")
            setup_ranger()
        case _:
            setup_fish()
            setup_tmux()
            setup_vim()
            setup_ranger()
            print("Bye.")


def main():
    select()

if __name__ == "__main__":
    main()
